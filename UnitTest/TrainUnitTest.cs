﻿using CircusTrein;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    [TestClass]
    public class TrainUnitTest
    {
        [TestMethod]
        public void AddAnimalToWagon()
        {
            //arrange
            Train train = new Train();
            Animal animal = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Medium);

            //act
            Wagon wagon = train.AddAnimal(animal);

            //assert
            Assert.AreEqual(1, train.wagons.Count);
            CollectionAssert.Contains(wagon.Animals, animal);
        }

        [TestMethod]
        public void AddMulipleAnimalToWagon()
        {
            //arrange
            Train train = new Train();
            Animal animal1 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Large);
            Animal animal2 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Large);          
            Animal animal3 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Large);

            //act
            train.AddAnimal(animal1);
            train.AddAnimal(animal2);
            train.AddAnimal(animal3);
             
            //assert
            Assert.AreEqual(3, train.wagons.Count);

            //nog een unittest naar wat Fred zei
        }

        [TestMethod]
        public void AddFredKudde1()
        {
            Train train = new Train();
            Animal animalH1 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Small);
            Animal animalH3 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Medium);
            Animal animalH5 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Large);

            //act
            train.AddAnimal(animalH5);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH3);

            //assert
            Assert.AreEqual(2, train.wagons.Count);
        }

        [TestMethod]
        public void AddFredKudde2()
        {
            Train train = new Train();
            Animal animalC1 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Small);
            Animal animalC3 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Medium);
            Animal animalC5 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Large);          
            Animal animalH5 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Large);

            //act
            train.AddAnimal(animalC5);
            train.AddAnimal(animalC5);
            train.AddAnimal(animalC3);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalC3);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalC3);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalC1);

            //assert
            Assert.AreEqual(6, train.wagons.Count);
        }

        [TestMethod]
        public void AddFredKudde3()
        {
            Train train = new Train();
            Animal animalC1 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Small);           
            Animal animalC3 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Medium);         
            Animal animalC5 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Large);           
            Animal animalH1 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Small);           
            Animal animalH3 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Medium);          
            Animal animalH5 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Large);

            //act
            train.AddAnimal(animalC5);
            train.AddAnimal(animalC5);
            train.AddAnimal(animalC3);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalC3);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalC1);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalC1);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH1);

            //assert
            Assert.AreEqual(8, train.wagons.Count);
        }

        [TestMethod]
        public void AddFredKudde4()
        {
            Train train = new Train();
            Animal animalC1 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Small);
            Animal animalC3 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Medium);
            Animal animalC5 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Large);
            Animal animalH1 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Small);
            Animal animalH3 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Medium);
            Animal animalH5 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Large);

            //act
            train.AddAnimal(animalC1);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalC1);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalC3);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalC3);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalC5);
            train.AddAnimal(animalC5);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH1);
            train.AddAnimal(animalH1);

            //assert
            Assert.AreEqual(8, train.wagons.Count);
        }

        [TestMethod]
        public void AddFredKudde5()
        {
            Train train = new Train();           
            Animal animalH1 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Small);
            Animal animalH3 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Medium);
            Animal animalH5 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Large);

            //act
            train.AddAnimal(animalH5);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH3);          
            train.AddAnimal(animalH1);

            //assert
            Assert.AreEqual(2, train.wagons.Count);
        }

        [TestMethod]
        public void AddFredKudde6()
        {
            Train train = new Train();
            Animal animalC1 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Small);
            Animal animalC3 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Medium);
            Animal animalC5 = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Large);
            Animal animalH1 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Small);
            Animal animalH3 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Medium);
            Animal animalH5 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Large);

            //act
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalH3);
            train.AddAnimal(animalC1);
            train.AddAnimal(animalH5);
            train.AddAnimal(animalH5);            

            //assert
            Assert.AreEqual(2, train.wagons.Count);
        }
    }
}
