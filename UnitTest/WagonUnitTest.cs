﻿using CircusTrein;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    [TestClass]
    public class WagonUnitTest
    {       
        [TestMethod]
        public void AddAnimalToWagon()
        {
            //arrange
            Wagon wagon = new Wagon();
            Animal animal = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Large);

            wagon.AddAnimal(animal);

            //assert
            Assert.AreEqual(5, wagon.CurrentCargo());
        }

        [TestMethod]
        public void AddMultipleAnimalsToWagon()
        {
            //arrange
            Wagon wagon = new Wagon();
            Animal LargeHerbivore = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Large);
            Animal MediumHerbivore = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Medium);
            Animal SmallCarnivore = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Small);
            Animal MediumHerbivore2 = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Medium);

            //act
            bool test1 = wagon.AddAnimal(LargeHerbivore);
            bool test2 = wagon.AddAnimal(MediumHerbivore);
            bool test3 = wagon.AddAnimal(SmallCarnivore);
            bool test4 = wagon.AddAnimal(MediumHerbivore2);

            //assert
            Assert.IsTrue(test1);
            Assert.IsTrue(test2);
            Assert.IsTrue(test3);

            Assert.AreEqual(9, wagon.CurrentCargo());

            Assert.IsFalse(test4);

            Assert.AreEqual(9, wagon.CurrentCargo());
        }
    }
}
