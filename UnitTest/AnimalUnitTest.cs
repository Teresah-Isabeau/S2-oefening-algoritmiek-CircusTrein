﻿using CircusTrein;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    [TestClass]
    public class AnimalUnitTest
    {
        [TestMethod]
        public void CannotAddLargeCarnivoreWithSmallCarnivore()
        {
            //arrange          
            Animal animal = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Small);

            Animal LargeCarnivore = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Large);

            //act
            bool test = LargeCarnivore.CanLiveWith(animal);

            Assert.IsFalse(test);
        }

        [TestMethod]
        public void CanAddLargeHerbivoreWithSmallHerbivore()
        {
            //arrange          
            Animal animal = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Small);

            Animal LargeHerbivore = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Large);

            //act
            bool test = LargeHerbivore.CanLiveWith(animal);           

            //assert
            Assert.IsTrue(test);
        }

        [TestMethod]
        public void CannotAddMediumHerbivoreWithMediumCarnivore()
        {
            //arrange           
            Animal animal = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Medium);

            Animal MediumCarnivore = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Medium);

            //act
            bool test = MediumCarnivore.CanLiveWith(animal);
           
            //assert
            Assert.IsFalse(test);
        }

        [TestMethod]
        public void CanAddMediumHerbivoreWithSmallCarnivore()
        {
            //arrange
            Animal animal = new Animal(Animal.AnimalType.Herbivore, Animal.AnimalSize.Medium);

            Animal SmallCarnivore = new Animal(Animal.AnimalType.Carnivore, Animal.AnimalSize.Small);


            //act
            bool test = SmallCarnivore.CanLiveWith(animal);
           
            //assert
            Assert.IsTrue(test);         
        }
    }
}
