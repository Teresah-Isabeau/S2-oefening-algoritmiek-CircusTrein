﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CircusTrein
{
    public partial class CircusTrein : Form
    {
        Train Train;
        public CircusTrein()
        {
            InitializeComponent();
            Train = new Train();
        }

        private void btnAddAnimal_Click(object sender, EventArgs e)
        {
            string Animaltype = cbAnimalType.SelectedItem.ToString(); //haalt de geselecteerde animaltype op
            string Animalsize = cbAnimalSize.SelectedItem.ToString();

            Animal animal = new Animal();//animal meegeven****
            animal.SetAnimalSizeByString(Animalsize); //haalt animalsize op
            animal.SetAnimalTypeByString(Animaltype);

            Wagon wagon = Train.AddAnimal(animal); //Voegt een animal toe met bovenstaande type en size
            lbRegisterdAnimals.Items.Add("Added " + Animalsize + " " + Animaltype + " to " + wagon.Name); //toont het toegevoegde dier en wagonnummer
            lbRegisterdAnimals.SelectedIndex = lbRegisterdAnimals.Items.Count - 1; //zorgt dat de list automatisch mee scrolled.. because I'm Lazy :D
        }
    }
}
