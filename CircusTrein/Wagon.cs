﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircusTrein
{
    public class Wagon
    {
        public string Name { get; set; }
        public int MaxCargo { get; set; }
        public List<Animal> Animals { get; set; }

        public Wagon(int cargo = 10)
        {
            MaxCargo = cargo;
            Animals = new List<Animal>();
        }

        public Wagon(string name, int cargo = 10)
        {
            Name = name;
            MaxCargo = cargo;
            Animals = new List<Animal>();
        }

        /// <summary>
        /// Voegt een animal toe aan een wagon
        /// </summary>
        /// <param name="animal"></param>
        /// <param name="wagon"></param>
        /// <returns></returns>
        public bool AddAnimal(Animal animal)
        {
            int currentCargoWithAnimal = this.CurrentCargo() + (int)animal.Animalsize; //berekend de totale cargo van de animal en de wagon
            if (currentCargoWithAnimal <= this.MaxCargo && CanAddType(animal)) //als currentcargo lager is dan 10 & CanAddType true is word de animal toegevoegd
            {
                this.Animals.Add(animal);
                return true;
            }
            return false; //returns false als de currentcargo hoger is dan 10
        }

        private bool CanAddType(Animal animal)
        {
            foreach (Animal animalInWagon in this.Animals)
            {
                if (animal.CanLiveWith(animalInWagon) == false)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// berekend de huidige cargo van een wagon
        /// </summary>
        /// <returns></returns>
        public int CurrentCargo()
        {
            int count = 0;
            foreach (Animal animal in this.Animals)
            {
                count += (int) animal.Animalsize;
            }

            return count;
        }
    }
}
