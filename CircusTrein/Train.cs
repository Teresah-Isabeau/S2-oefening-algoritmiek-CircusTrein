﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircusTrein
{
    public class Train
    {
        public List<Wagon> wagons { get; set; }

        public Train()
        {
            wagons = new List<Wagon>();        
        }

        /// <summary>
        /// voegt een animal toe
        /// </summary>
        /// <param name="animalType"></param>
        /// <param name="animalSize"></param>
        /// <returns></returns>
        public Wagon AddAnimal(Animal animal)
        {
            bool inserted = false;
            foreach (Wagon wagon in this.wagons) //loop door alle aangemaakte wagons
            {                      
                inserted = wagon.AddAnimal(animal); //probeerd een animal toe te voegen
                if (inserted) //als insert is gelukt return hij de wagon om de zien ik welke wagon het dier zit
                {
                    return wagon;
                }                                           
            }
            //als insert false is word er een nieuwe wagon gemaakt en voegt het dier aan deze toe
            Wagon newWagon = AddNewWagon();
            newWagon.AddAnimal(animal);
            return newWagon;
        }
        

        /// <summary>
        /// voegt een nieuwe wagon toe
        /// </summary>
        /// <returns></returns>
        private Wagon AddNewWagon()
        {
            string name = "Wagon " + Convert.ToString(this.wagons.Count + 1); //geeft een naam aan de wagon

            Wagon wagon = new Wagon(name);
            this.wagons.Add(wagon);

            return wagon;
        }        
    }
}
