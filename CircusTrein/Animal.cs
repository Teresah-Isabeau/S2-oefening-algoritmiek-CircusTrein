﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircusTrein
{
    public class Animal
    {
        public AnimalType Animaltype { get; set; }
        public AnimalSize Animalsize { get; set; }
        
        public Animal()
        {

        }

        public Animal(AnimalType animalType, AnimalSize animalSize)
        {
            this.Animaltype = animalType;
            this.Animalsize = animalSize;
        }

        public enum AnimalType
        {
            Herbivore,
            Carnivore
        }

        public enum AnimalSize
        {
            Small = 1,
            Medium = 3,
            Large = 5
        }

        /// <summary>
        /// geeft animaltype als string door
        /// </summary>
        /// <param name="animaltype"></param>
        public void SetAnimalTypeByString(string animaltype)
        {
            Animaltype = GetAnimalType(animaltype);
        }

        /// <summary>
        /// geeft animalsize als string door
        /// </summary>
        /// <param name="animalsize"></param>
        public void SetAnimalSizeByString(string animalsize)
        {
            Animalsize = GetAnimalSize(animalsize);
        }

        /// <summary>
        /// methode om de animal type te bepalen
        /// </summary>
        /// <param name="animaltype"></param>
        /// <returns></returns>
        private AnimalType GetAnimalType(string animaltype)
        {
            string selectedItem = animaltype;

            if (selectedItem == "Herbivore")
            {
                return Animal.AnimalType.Herbivore;
            }
            else
            {
                return Animal.AnimalType.Carnivore;
            }
        }

        /// <summary>
        /// methode om de animal size te bepalen
        /// </summary>
        /// <param name="animalsize"></param>
        /// <returns></returns>
        private AnimalSize GetAnimalSize(string animalsize)
        {
            string selectedItem = animalsize;

            if (selectedItem == "Small")
            {
                return Animal.AnimalSize.Small;
            }
            else if (selectedItem == "Medium")
            {
                return Animal.AnimalSize.Medium;
            }
            else
            {
                return Animal.AnimalSize.Large;
            }
        }

        public bool CanLiveWith(Animal animal)
        {                     
            if (Animaltype == AnimalType.Herbivore)
            {
                if (animal.Animaltype == AnimalType.Carnivore && this.Animalsize <= animal.Animalsize)
                {
                    return false;
                }
            }

            if (Animaltype == AnimalType.Carnivore)
            {
                if (animal.Animaltype == AnimalType.Herbivore && this.Animalsize >= animal.Animalsize)
                {
                    return false;
                }

                if (animal.Animaltype == AnimalType.Carnivore)
                {
                    return false;
                }
            }           
            return true;
        }
    }
}
